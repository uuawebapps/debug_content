# Debug Content with Styles and JavaScript

## Overview

CSS and JS files with instructions for use as an accessibility, usability, and house styles debugging tool for content authors and site developers. Designed as a module for use in Drupal 9, but scripts and styles should work in other sites with custom installation.

The Debug Content module is a very user-friendly content-focused debugging tool for non-developers (though it does require some development skills for installation and, if desired, customization). JavaScript allows us to debug content problems that couldn't be identified by CSS alone, and helps provide specific and helpful error messaging to help content authors fix any issues the debugger finds.

Debugging notes are separated into three categories of seriousness: success (things that may be okay but require a human check), warning (things that are probably not okay but require a human check), and danger (problems that should be fixed). Although the CSS color-codes these levels with green, yellow, and orange respectively, the wording of each note helps indicates its seriousness: successes and warnings are usually phrased as questions while danger notes are informative or take the form of instructions.

## Similar Solutions

There are several CSS-only projects designed to help developers discover invalid or inaccessible code. They were a great inspiration and starting point to this project:

* [a11y.css](https://github.com/ffoodd/a11y.css)
* [REVENGE.CSS](https://github.com/Heydon/REVENGE.CSS)
* [debugCSS](https://github.com/imbrianj/debugCSS)
* [debug.css](https://github.com/nternetinspired/debug-css)

The [Editoria11y Accessibility Checker](https://www.drupal.org/project/editoria11y) is a user-friendly content-focused checker for Drupal. It covers fewer/different issues than Debug Content, but is really slick, with explanations of problems and solutions as well as links to more information. It also has a nice interface for site admins to define areas to review or ignore.

### How Debug Content is Different

This code was built and is maintained for my organization's use case. Hopefully the commentary on `js/debug_content.js` and self-descriptive psuedo-content for the elements marked by `scss/debug_content.scss` are enough to guide your own modifications or translations from English.

Some of the cooler features and/or areas that might need customization for your use case include:

* The social media preview is based on the page's Open Graph meta tags. You may need to add those meta tags or adjust the script accordingly.
* We ignore code that isn't allowed on our sites. The CSS projects listed above may have more code that's relevant to your use case, especially if you're likely to find deprecated or improper HTML elements.
* The debugger checks internal links for 403 or 404 errors and marks them up inline. If you have or want to set up a CORS proxy you could mark up external links the same way. This script builds a list of external links at the bottom of the page with instructions to check them manually instead.
* We check for duplicate links, link text that is too short or too long, mailto links that don't look like email addresses, links to audio or video that will play automatically, and links to PDFs or other non-standard web pages that aren't labeled to let people know what to expect. If you have different preferences for marking links like those, you will need to adjust the relevant portions of the script.
* Our editorial standards are also reflected in things like marking up mis-capitalized headings (with exceptions for some acronyms specific to our audience), paragraphs that have more than 200 words, or paragraphs with phrases that could be considered distracting "fluff" words. 

## Components

* This module uses minified CSS by default, but origin SCSS and uncompressed CSS files are included for customization.
* JavaScript helps to parse code and flag problems that CSS alone can't.
* Simple text links can be used as toggles to turn the debugging styles on or off - use the included Drupal blocks or see the "Custom Installation" section for non-Drupal instructions.

## Installation

### Composer Installation (for Drupal)

These instructions will let you use this Drupal module as-is, easily updateable from the source files. For minor customization, you could overwrite styles with CSS on your site, or create and apply locally-stored [patch files](https://www.drupal.org/docs/develop/git/using-git-to-contribute-to-drupal/making-a-patch).

1. In your site's `composer.json`, include the following package in the `repositories` section:
```
  "repositories": [
    {
      "type": "package",
      "package": {
        "name": "uuawebapps/debug_content",
        "version": "3.1",
        "type": "drupal-module",
        "dist": {
          "type": "zip",
          "url": "https://bitbucket.org/uuawebapps/debug_content/get/release/v3.1.zip"
        }
      }
    }
  ]
```
2. Install the Debug Content module: `composer require uuawebapps/debug_content`
3. Enable the module: `drush en debug_content`
4. Place one or both of the "Debugging Content" blocks within your theme - "Debug content" is for areas content authors can control, and "Debug site" targets site-wide elements; restrict access to these blocks accordingly to avoid unecessary styles and scripts from being loaded for users who don't need them. These blocks also attach the necessary CSS/JS files. Remember to export your configuration files (`drush cex`).
5. Make sure your public theme's pages have a `<main>` element containing the page's content (debugging info will appear at the top and bottom of it) and add the "debug-author-content" class to any section of content that authors can control by editing the page:
    - If you're using a *custom* default theme, check its `templates/layout/page.html.twig` file for a `<main>` element. You can edit that file to add the "debug-author-content" class to author-controlled elements. For example: 
    ```
    <div class="layout-content">
      {{ page.content }}
    </div>
    ```
    would become
    ```
    <div class="layout-content debug-author-content">
      {{ page.content }}
    </div>
    ```
    - Drupal's *core* themes all have a `<main>` element, and there are a number of ways to add the "debug-author-content" class where you need it, including:
        - Use the [Block Class](https://www.drupal.org/project/block_class) module to add the class to any page content blocks.
        - [Create a sub-theme](https://www.drupal.org/docs/theming-drupal/creating-sub-themes) so you can [override Drupal core templates](https://www.drupal.org/docs/theming-drupal/twig-in-drupal/working-with-twig-templates#s-overriding-templates) and make your own copy of `templates/layout/page.html.twig` to edit as needed.
        - [Create a custom module](https://mikemadison.net/blog/2020/7/27/an-absolute-beginners-guide-writing-your-first-drupal-9-module) or edit this one (either as a custom-installed module or with a patch) to add a `hook_preprocess_HOOK()` function like either of the samples in `debug_content.module`.

To update from a previous version, change the `"version"` and `"url"` values to the version you want from [debug_content branches](https://bitbucket.org/uuawebapps/debug_content/downloads/?tab=branches), and re-run the `composer require uuawebapps/debug_content` command.

### Custom or Non-Drupal Installation

To make more extensive modifications for your own use-case, download and install the module manually. 

1. Download the release with the highest version number from [debug_content branches](https://bitbucket.org/uuawebapps/debug_content/downloads/?tab=branches).
2. Expand the folder and place it with your other custom modules or extensions.
3. In Drupal, enable the module: `drush en debug_content`. For non-Drupal sites, continue.
4. If you're using Drupal 9, follow steps 4+ from the Composer Installation section, above. If you're _not_ using Drupal or prefer not to use the included custom blocks:
    1. Load `js/debug_content.js` and  `css/debug_content.min.css` when you'll need them (ideally, only for your logged-in content authors) 
    2. Add toggles so you and your team can turn the debugging styles on/off: `js/debug_content.js` looks for the `debug-content-toggle` or `debug-site-toggle` class on clickable objects like the following links with the "button" role:
        - Content debugging for page authors: `<a href="#" class="debug-content-toggle" role="button" title="Toggle debugging styles for content">Debug content</a>`
        - Site debugging for admins: `<a href="#" class="debug-site-toggle" role="button" title="Toggle debugging styles for site elements">Debug site</a>`
    3. Make sure your page template includes a `<main>` element containing the main content area; debugging info will appear at the top and bottom of it.
    4. Add the "debug-author-content" class to any section that authors can control by editing the page.
    5. Read through the commentary/code on `js/debug_content.js` and `scss/debug_content.scss` to find areas you may need/want to customize for your use case.

#### Updating the Styles

* **With SASS**: Install [SASS](https://sass-lang.com/) to compile SCSS to CSS. From the debug_content folder:
    - Update the minified CSS: `sass scss/debug_content.scss:css/debug_content.min.css --style compressed`
    - Use the `--watch` flag to re-compile automatically as you work.
    - If you want to update the unused uncompressed CSS: `sass scss/debug_content.scss:css/debug_content.css`
* **Without SASS**: Load `css/debug_content.css` instead of `css/debug_content.min.css` on your pages (in Drupal, update the css reference in `debug_content.libraries.yml`) and make your updates to the uncompressed CSS file (`css/debug_content.css`).

## Maintainance

This module is maintained by [Kasey Melski Kruser](https://www.uua.org/offices/people/kasey-kruser) on behalf of the [Unitarian Universalist Association](https://www.uua.org).
