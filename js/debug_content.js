/**
 * Toggle a body class which will expose debugging CSS for staff or admins.
 * Assess page elements and add classes or attributes to aid in debugging.
 */

  // Toggle debugging styles on certain triggers.
  document.addEventListener("DOMContentLoaded", () => {
    // Turn on debugging when coming from node edit/create or admin/content URLs.
    const editReferrers = ['/edit', '/node/add', '/clone', '/admin/content'];
    if (new RegExp(editReferrers.join('|')).test(document.referrer)) {
      document.body.classList.toggle('debug-content');
      debug();
    } 
    // Toggle page debugging when that toggle is clicked.
    document.querySelector('.debug-content-toggle').addEventListener('click', (e) => {
      document.body.classList.toggle('debug-content');
      document.body.classList.remove('debug-site');
      debug();
      e.stopImmediatePropagation();
      return false;
    });
    // Toggle site debugging when that toggle is clicked.
    document.querySelector('.debug-site-toggle').addEventListener('click', (e) => {
      document.body.classList.toggle('debug-site');
      document.body.classList.remove('debug-content');
      e.stopImmediatePropagation();
      return false;
    });
  });

  // Use javascript to do more than CSS can do alone.
  function debug() {
    // Add a div showing information about debugging.
    // Build the div if it doesn't already exist.
    if (document.querySelectorAll('#debug-info').length == 0) {
      const debugDiv = document.createElement('div');
      debugDiv.setAttribute('id', 'debug-info');
      debugDiv.classList.add('m-3', 'p-1', 'debug-only', 'text-black', 'bg-success');
      debugDiv.innerHTML = '<strong>Debugging on</strong>: review highlighted concerns, check images for descriptive alt text, and confirm everything has contrast & meaning without color.<br>Use the “Debug content” link to toggle debugging styles and messages.';
      // Add debugging info before the main element.
      document.querySelector('main').before(debugDiv);
    } // debug info
    // Add a div showing social media share information.
    // Build the div if it doesn't already exist.
    if (document.querySelectorAll('#debug-share').length == 0) {
      // Prepare the div with an id and some classes.
      const shareDiv = document.createElement('div');
      shareDiv.setAttribute('id', 'debug-share');
      shareDiv.classList.add('m-3', 'debug-only', 'border', 'border-success', 'bg-white');
      // Get content from the page's meta tags for social media.
      const shareUrl = document.querySelector('meta[property="og:url"]') ? document.querySelector('meta[property="og:url"]').content : '';
      const shareTitle = document.querySelector('meta[property="og:title"]') ? document.querySelector('meta[property="og:title"]').content : '';
      const shareSummary = document.querySelector('meta[property="og:description"]') ? document.querySelector('meta[property="og:description"]').content : '';
      const shareImageAlt = document.querySelector('meta[property="og:image:alt"]') ? document.querySelector('meta[property="og:image:alt"]').content : '';
      const shareImage = document.querySelector('meta[property="og:image"]') ? document.querySelector('meta[property="og:image"]').content : '';
      // Get the filename of the shared image without tokens.
      var shareImageName = /[^/]*$/.exec(shareImage)[0];
      shareImageName = shareImageName.split(/[?]/, 1)[0];
      // Add a message if the shared image is not used elsewhere on the page.
      var shareImageNotUsed = ' <strong>This image isn\'t visible on this page.</strong> Add it (preferably near the top) to help people know they\'re in the right spot.';
      // Don't show the message if the share image is the site default (filename includes 'logo_share') or is used on the page.
      if (shareImageName.includes('logo_share') == true) {
        shareImageNotUsed = '';
      }
      else if (document.querySelectorAll('.debug-author-content').length > 0) {
        const debugContentNotProcessed = document.querySelectorAll('.debug-author-content:not(.processed)');
        for (let i=0; i<debugContentNotProcessed.length; i++) {
          const thisDebugContent = debugContentNotProcessed[i];
          // Add a "processed" class so we don't loop.
          thisDebugContent.classList.add('processed');
          if (thisDebugContent.innerHTML.includes(shareImageName) == true) {
            shareImageNotUsed = '';
          }
        }
      } 
      // Build the share preview with content and messages gathered above.
      shareDiv.innerHTML = '<h2 class="bg-success text-black m-0 p-1">Sharing to Social Media</h2><p class="m-3 font-italic">Make sure this is the right image and title/summary for social media, then check the <a href="https://developers.facebook.com/tools/debug/sharing?q=' + shareUrl + '">Facebook</a> preview tool before sharing.' + shareImageNotUsed + '</p><div class="d-flex flex-wrap m-3"><div class="col-3"><img src="' + shareImage + '" style="width:100%;height:auto;" title="Hook image"><span class="bg-text text-white p-1 debug-only">Alt text: ' + shareImageAlt + '</span></div><div class="col-9"><strong title="Title for social media">' + shareTitle + '</strong><br><span title="Summary for social media">' + shareSummary + '</span></div></div>';
      // Add social media info before the main element.
      document.querySelector('main').before(shareDiv);
    } // social media preview
    // Prepare empty arrays for link URLs and text so we can look for duplicates later.
    const linkUrls = [];
    const linkTexts = [];
    // For each link within .debug-author-content areas without the "processed" class or the "button" role or some aria- attributes (commonly, images linking to the same URL as a title).
    const allLinks = document.querySelectorAll('.debug-author-content a:not(.processed):not([role="button"]):not([onclick]):not([aria-describedby]):not([aria-label]):not([aria-labelledby])');
    for (let i=0; i<allLinks.length; i++) {
      const thisLink = allLinks[i];
      // Add a "processed" class so we don't loop.
      thisLink.classList.add('processed');
      // Get the domain from the URL.
      let thisDomain = (new URL(thisLink.href)).hostname.replace('www.','');
      // If the URL isn't in linkUrls, add it; else mark it as a duplicate URL.
      if (linkUrls.indexOf(thisLink.href) < 0) {
        linkUrls.push(thisLink.href);
      }
      else {
        thisLink.classList.add('duplicate-link');
      }
      // If the inner text isn't in linkTexts, add it; else mark it as duplicate text.
      if (linkTexts.indexOf(thisLink.innerText.toLowerCase().trim()) < 0) {
        linkTexts.push(thisLink.innerText.toLowerCase().trim());
      }
      else {
        thisLink.classList.add('duplicate-link-text');
      }
      // Build a list of external links where fetch doesn't work (on http URLs) or returns a not-ok response (external URLs).
      if ((thisLink.href.indexOf('mailto:') === -1) && (thisLink.href.indexOf('tel:') === -1) ){
        fetch(thisLink.href, { mode: 'no-cors', credentials: 'omit' }).then(function(res) {
          // res instanceof Response == true.
          if (!res.ok) {
            // For external links, build a list to print at page bottom for review.
            if (res.status == 0) {
              buildExternalLinksList(thisLink, i);
            }
            // For all not-ok responses, add a class to the links.
            thisLink.classList.add('error-' + res.status);
          }
        }).catch((error) => {
          // Add http links to the list too.
          buildExternalLinksList(thisLink, i);
        });
      }
      // Get the URL without anchors or arguments.
      const baseLink = thisLink.href.split(/[?#]/, 1)[0];
      // Get everything after the last slash.
      const pathEnd = baseLink.split(/[\/]+/).pop();
      // Get everything after the last period.
      var ext = null;
      if (pathEnd.indexOf('.') > 0) {
        ext = pathEnd.split(/[\.]+/).pop();
        // If the extension doesn't contain any letters, ignore it.
        if (!/[a-z]/i.test(ext)) {
          ext = null;
        }
      }
      // Prepare an array of non-standard-HTML page descriptions and format notes for links with file extensions (default to just the extension).
      const extDescription = [];
      const formatNote = [];
      if (ext) {
        switch(ext) {
          // Ignore standard web page extensions.
          case 'asp':
          case 'aspx':
          case 'html':
          case 'php':
          case 'shtml':
          case 'xhtml':
            break;
          case 'doc':
          case 'docx':
            // Links to doc files must include "doc" or "word" within their link text so people know what to expect.
            extDescription.push('doc', 'word');
            // Our CSS will include this in the tip about how to change the text to be more informative.
            formatNote.push('"(DOC)" or "(Word, # pages)"');
            // Add this link to our list reminding people to check documents for accessibility.
            buildDocumentLinksList(thisLink);
            break;
          case 'pdf':
            extDescription.push('pdf');
            formatNote.push('"(PDF)" or "(PDF, # pages)"');
            buildDocumentLinksList(thisLink);
            break;
          case 'ppt':
          case 'pptx':
            extDescription.push('ppt', 'powerpoint');
            formatNote.push('"(PPT)" or "(PowerPoint, # pages)"');
            buildDocumentLinksList(thisLink);
            break;
          case 'xls':
          case 'xlsx':
            extDescription.push('xls', 'excel');
            formatNote.push('"(XLS)" or "(Excel, # pages)"');
            buildDocumentLinksList(thisLink);
            break;
          case 'mp3':
            extDescription.push('mp3', 'audio');
            formatNote.push('("MP3)" or "(Audio, # minutes)"');
            buildAudioLinksList(thisLink);
            break;
          case 'mp4':
            extDescription.push('mp4', 'video');
            formatNote.push('"(MP4)" or "(Video, # minutes)"');
            buildVideoLinksList(thisLink);
            break;
          case 'epub':
            extDescription.push('epub');
            formatNote.push('"(EPUB)" or "(EPUB, # pages)"');
            buildDocumentLinksList(thisLink);
            break;
          case 'gif':
            extDescription.push('gif', 'image');
            formatNote.push('"(GIF)" or "(Image, # pixels)"');
            buildImageLinksList(thisLink);
            break;
          case 'jpg':
          case 'jpeg':
            extDescription.push('jpg', 'image');
            formatNote.push('"(JPG)" or "(Image, # pixels)"');
            buildImageLinksList(thisLink);
            break;
          case 'png':
            extDescription.push('png', 'image');
            formatNote.push('"(PNG)" or "(Image, # pixels)"');
            buildImageLinksList(thisLink);
            break;
          case 'svg':
            extDescription.push('svg', 'image');
            formatNote.push('"(SVG)"');
            buildImageLinksList(thisLink);
            break;
          case 'wav':
            extDescription.push('wav', 'audio');
            formatNote.push('"(WAV)" or "(Audio, # seconds)"');
            buildAudioLinksList(thisLink);
            break;
          default:
            extDescription.push(ext);
            formatNote.push('"(' + ext.toUpperCase() + ')"');
            break;
        }
        // Set a "format-note" attribute we can use in the error messaging our CSS will display.
        thisLink.setAttribute('format-note', formatNote);
      } // ext found
      // If we know what the file format is, check that the link text includes it.
      if (extDescription.length > 0) {
        regex = new RegExp(extDescription.join('|'));
        // Mark links to non-standard web pages that don't inform people about that within the link text.
        if (regex.test(thisLink.innerText.toLowerCase()) == false) {
          thisLink.classList.add('format-link');
        }
        // Remove the note about duplicate link text from links like "(PDF)" unless it's also a duplicate link.
        // Multiple docs with multiple formats only need full description on first link in series, e.g., "<a>Document 1 (Word)</a> (<a>PDF</a>)"
        if (thisLink.innerText.toLowerCase().replace('/\(|\)/', '').includes(extDescription)) {
          if (!thisLink.classList.contains('duplicate-link')) {
            thisLink.classList.remove('duplicate-link-text');
          }
        }
      }
      // If we didn't find an extension, check for YouTube/Vimeo links.
      else if ((thisDomain.toLowerCase().search(/vimeo\.?/) !== -1) && (thisLink.innerHTML.toLowerCase().search(/vimeo|video/) == -1)) {
        thisLink.classList.add('format-link');
        thisLink.setAttribute('format-note', '"(Vimeo)" or "(Video, # minutes)"');
        buildVideoLinksList(thisLink);
      }
      else if ((thisDomain.toLowerCase().search(/youtu\.?be\.?/) !== -1) && (thisLink.innerHTML.toLowerCase().search(/youtube|video/) == -1)) {
        thisLink.classList.add('format-link');
        thisLink.setAttribute('format-note', '"(YouTube)" or "(Video, # minutes)"');
        buildVideoLinksList(thisLink);
      }
      // Mark links with text explaining how links work (e.g., "click here").
      const explainingLinks = ['here', 'click here', 'link', 'go to', 'visit'];
      if (new RegExp(explainingLinks.join('|')).test(thisLink.innerText.toLowerCase())) {
        thisLink.classList.add('explaining-link');
        // Set an "explaining" attribute with the (first) offending word/phrase - for use in error messaging.
        thisLink.setAttribute('explaining', thisLink.innerText.toLowerCase().match(explainingLinks.join('|'))[0]);
      }
      // Mark links with poor descriptive text (too short).
      if (thisLink.innerHTML.length < 3) {
        thisLink.classList.add('short-link');
      }
      // Mark links with poor descriptive text (too long).
      else if ((thisLink.innerText.length > 80) && (thisLink.classList.contains('no-line') === false)) {
        thisLink.classList.add('long-link');
      }
      // Mark mailto links that don't show as an email address.
      if ((thisLink.innerText.indexOf('@') === -1) && (thisLink.href.indexOf('mailto:') !== -1)) {
        thisLink.classList.add('email-link');
      }
      // Ignore mailto and tel links; check for link text containing the domain from the href or text matching a URL-like pattern (to account for cases like an href to bit.ly and text like Google.com).
      if ((thisLink.href.indexOf('mailto:') === -1) && (thisLink.href.indexOf('tel:') === -1) && ((thisLink.innerText.toLowerCase().indexOf(thisDomain.toLowerCase()) !== -1) || (thisLink.innerText.match(/http|:\/\/|[-\w]+\.(?:[-\w]+\.xn--[-\w]+|[-\w]{2,3})$/)))) {
        thisLink.classList.add('url-link');
      }
    }; // links
    // For each img within .debug-author-content areas without the "processsed" class, display the alt attribute as an external caption.
    const imgNotProcessed = document.querySelectorAll('.debug-author-content img:not(.processed)');
    for (let i=0; i<imgNotProcessed.length; i++) {
      const thisImg = imgNotProcessed[i];
      // Add a "processed" class so we don't loop.
      thisImg.classList.add('processed');
      // Get caption from alt attribute.
      const captiontext = thisImg.getAttribute('alt');
      // Wrap the image in a span which won't break any text aligns on the parent contatiner.
      let wrap = document.createElement('span');
      wrap.classList.add('image-caption-container');
      // Get the parent element and put the wrapping element within it.
      const thisParent = thisImg.parentNode;
      thisParent.insertBefore(wrap, thisImg);
      wrap.appendChild(thisImg);
      // Append caption
      let altTxt = document.createElement('span');
      altTxt.setAttribute('class', 'bg-text text-white p-1 debug-only');
      altTxt.innerHTML = `Alt text: ${captiontext}`;
      wrap.appendChild(altTxt);
    }; // images
    
    // For each description list within .debug-author-content areas, check for both dt and dd elements.
    const descListNotProcessed = document.querySelectorAll('.debug-author-content dl:not(.processed)');
    for (let i=0; i<descListNotProcessed.length; i++) {
      const thisDl = descListNotProcessed[i];
      // Add a "processed" class so we don't loop.
      thisDl.classList.add('processed');
      // Add a class for any <dl> missing either <dd> or <dt> elements.
      if ((thisDl.querySelector('dt') == null) || (thisDl.querySelector('dd') == null)) {
        thisDl.classList.add('invalid-dl');
      }
    }; // description lists
    // For each paragraph within .debug-author-content areas.
      const pNotProcessed = document.querySelectorAll('.debug-author-content p:not(.processed)');
      for (let i=0; i<pNotProcessed.length; i++) {
        const thisParagraph = pNotProcessed[i];
        // Add a "processed" class so we don't loop.
        thisParagraph.classList.add('processed');
        // Paragraphs that are entirely bold (note we use "<strong>" and not "<b>" tags) should probably be headings or description lists.
        if ((thisParagraph.innerHTML.substring(0,7) == '<strong') && (thisParagraph.innerHTML.slice(thisParagraph.innerHTML.length - 9) == '</strong>')) {
          thisParagraph.classList.add('should-be-heading');
        }
        // Paragraphs starting with a number, asterisk, or dash should probably be marked as a list (<ul> or <ol>).
        if ((thisParagraph.innerText.trim().substring(0,1).match(/(\*|-){1}/)) || (thisParagraph.innerText.trim().substring(0,2).match(/([0-9]){1,2}(\.|\)){1}/))) {
          thisParagraph.classList.add('should-be-list');
        }
        // Paragraphs with more than 200 words should be broken up if possible.
        if (thisParagraph.innerText.split(' ').length > 200) {
          thisParagraph.classList.add('wall-of-text');
      }
      // Check for filler or "fluff" words/phrases unless we're on a page with an exempted body class.
      const fluffWords = ['here you\'ll find', 'here you will find', 'this page', 'welcome!', 'welcome to', 'please find', 'in order to'];
      // Body classes for content types allowed to have "fluff" (e.g., poetry or worship elements). 
      const fluffExemptions = ['node-worship-element'];
      if (document.body.getAttribute('class').search(fluffExemptions.join('|')) == -1) {
        if (new RegExp(fluffWords.join('|')).test(thisParagraph.innerText.toLowerCase())) {
          thisParagraph.classList.add('might-be-fluff');
          // Set a "fluff" attribute with the (first) offending phrase - for use in error messaging.
          thisParagraph.setAttribute('fluff', thisParagraph.innerText.toLowerCase().match(fluffWords.join('|'))[0]);
        }
      }
    }; // paragraphs
    // For each audio player within .debug-author-content areas.
    const audioNotProcessed = document.querySelectorAll('.debug-author-content audio source:not(.processed)');
    for (let i=0; i<audioNotProcessed.length; i++) {
      const thisAudio = audioNotProcessed[i];
      // Add a "processed" class so we don't loop.
      thisAudio.classList.add('processed');
      const audioFileName = /[^/]*$/.exec(thisAudio.src)[0].split(/[?]/, 1)[0];
      const thisLink = document.createElement('a');
      thisLink.setAttribute('href', `${thisAudio.src}`); thisLink.innerText = `Audio player for ${audioFileName}`;
      // Add embedded audio to the list of items to check for accessibility.
      buildAudioLinksList(thisLink);
    }; // audio players
    // For each video player within .debug-author-content areas.
    const videoNotProcessed = document.querySelectorAll('.debug-author-content iframe:not(.processed)');
    for (let i=0; i<videoNotProcessed.length; i++) {
      const thisVideo = videoNotProcessed[i];
      // Add a "processed" class so we don't loop.
      thisVideo.classList.add('processed');
      if (thisVideo.src.toLowerCase().search(/(vimeo\.?)|(youtu\.?be\.?)/) !== -1) {
        const videoUrl = decodeURIComponent(thisVideo.src.split('?url=')[1]).split('&')[0];
        const thisLink = document.createElement('a');
        thisLink.setAttribute('href', `${videoUrl}`); thisLink.innerText = `${thisVideo.title.replace('iFrame', 'Video player')}`;
        if (thisLink.innerText == '') {
          thisLink.innerText = `${'Video player for ' + thisLink.href}`;
        }
        // Add embedded video to the list of items to check for accessibility.
        buildVideoLinksList(thisLink);
      }
    }; // video players
    // For each incident of bolded text within .debug-author-content areas.
    const boldNotProcessed = document.querySelectorAll('.debug-author-content strong:not(.processed)');
    for (let i=0; i<boldNotProcessed.length; i++) {
      const thisBold = boldNotProcessed[i];
      // Add a "processed" class so we don't loop.
      thisBold.classList.add('processed');
      if (thisBold.innerText.length > 80) {
        thisBold.classList.add('long-strong');
      }
    }; // bold text
    // Build an array of headings so we can make sure they descend in order.
    const headings = [];
    var count = 0;
    const headingNotProcessed = document.querySelectorAll('.debug-author-content h2:not(.processed), .debug-author-content h3:not(.processed), .debug-author-content h4:not(.processed), .debug-author-content h5:not(.processed), .debug-author-content h6:not(.processed)');
    for (let i=0; i<headingNotProcessed.length; i++) {
      const thisHeading = headingNotProcessed[i];
      // Add a "processed" class so we don't loop.
      thisHeading.classList.add('processed');
      if (!thisHeading.sequence) {
        thisHeading.setAttribute('sequence', 'heading_' + i);
        headings['heading_' + i] = thisHeading.outerHTML.substring(2,3);
        if (thisHeading.innerText.length > 80) {
          document.querySelector('[sequence="heading_' + i + '"]').classList.toggle('long-heading');
        }
        // Check that titles/subtitles are using Title Case (unless they have the "text-uppercase" class).
        if (!thisHeading.classList.contains('text-uppercase') && (thisHeading.innerText !== toTitleCase(thisHeading.innerText))) {
          // Check for a subtitle - a <span> within the heading - which may have its own Title Case.
          var subtitle = '';
          const headingHTML = thisHeading.innerHTML;
          let parser = new DOMParser();
          let headingDOM = parser.parseFromString(headingHTML, 'text/html');
          headingDOM.querySelectorAll('span').forEach(function(s){
            subtitle = s.innerText.trim();
          });
          if (subtitle !== '') {
            const title = thisHeading.innerText.replace(subtitle, '');
            if ((title !== toTitleCase(title)) || (subtitle !== toTitleCase(subtitle))) {
              document.querySelector('[sequence="heading_' + i + '"]').classList.add('not-title-case');
            }
          }
          else {
            document.querySelector('[sequence="heading_' + i + '"]').classList.add('not-title-case');
          }              
        }
        // Headings starting with a number, asterisk, or dash should probably be part of a list (<ul> or <ol>).
        if ((thisHeading.innerText.trim().substring(0,1).match(/(\*|-){1}/)) || (thisHeading.innerText.trim().substring(0,2).match(/([0-9]){1,2}(\.|\)){1}/))) {
          document.querySelector('[sequence="heading_' + i + '"]').classList.add('should-be-list');
        }
        count = i;
      }
    };
    if (count > 0) {
      // Add a class if the first heading is not an H2.
      if (headings['heading_0'] !== '2') {
        document.querySelector('[sequence="heading_0"]').classList.add('should-be-h2');
      }
      // Use +2 so 'step' matches the heading number, starting with the second one.
      for (let step = 2; step < (count + 2); step++) {
        const difference = headings['heading_' + step] - headings['heading_' + (step - 1)];
        if ((difference > 1)) {
          const shouldBe = 'should-be-h' + (parseInt(headings['heading_' + (step - 1)], 10) + 1);
          document.querySelector('[sequence="heading_' + step + '"]').classList.add(shouldBe);
        }
      }
    } // headings
  } // debug

  // Change string to title case.
  function toTitleCase(thisTitle) {
    var i, j, str, lowers, uppers, uppersReplace;
    // These words should be left lowercase unless they are the first or last words in the string. Following MLA rules.
    lowers = ['A', 'About', 'After', 'Against', 'Along', 'An', 'And', 'Around', 'As', 'At', 'Between', 'But', 'By', 'For', 'From', 'In', 'Into', 'Near', 'Of', 'On', 'Onto', 'Or', 'Nor', 'So', 'The', 'To', 'With', 'Without', 'Yet'];
    // These words (initialisms or acronyms) should be mixed- or uppercase. The following two arrays need to be of equal length; the items in 'uppers' will be replaced by the items in 'uppersReplace' according to the order in which they appear.
    uppers = ['Covid', 'Ga', 'Hr', 'Leaderlab', 'Lgbtq', 'Lgbtq+', 'Uplift', 'Uu', 'Uua', 'Uus', 'Uuworld', 'Worshiplab', 'Worshipweb'];
    uppersReplace = ['COVID', 'GA', 'HR', 'LeaderLab', 'LGBTQ', 'LGBTQ+', 'UPLIFT', 'UU', 'UUA', 'UUs', 'UUWorld', 'WorshipLab', 'WorshipWeb'];
    str = thisTitle.replace(/([^\W_]+[^\s]*) */g, function(txt) {
      return txt.charAt(0).toUpperCase() + txt.substring(1).toLowerCase();
    });
    for (i = 0, j = lowers.length; i < j; i++)
      str = str.replace(new RegExp('\\s' + lowers[i] + '\\s', 'g'), 
        function(txt) {
          return txt.toLowerCase();
        });
    for (i = 0, j = uppers.length; i < j; i++)
      str = str.replace(new RegExp('\\b' + uppers[i] + '\\b', 'g'), 
      uppersReplace[i]);      
    return str;
  } // toTitleCase

  // A function to build a list of external links.
  function buildExternalLinksList(thisLink, i) {
    // Get the link href and text.
    const thisHref = thisLink.getAttribute('href');
    // Ignore mailto and tel links.
    if ((thisHref.indexOf('mailto:') === -1) && (thisHref.indexOf('tel:') === -1)) {
      const thisText = thisLink.innerText.trim().replace('Alt text:', 'Image with alt text:');
      // Build the containing div and list if it doesn't exist.
      var externalLinksUl = document.getElementById('external-links');
      if (externalLinksUl === null) {
        let div = document.createElement('div');
        div.innerHTML = `<div class="debug-only border border-success mb-3"><h2 class="bg-success text-black m-0 p-1">External Links</h2><p class="m-3">Make sure the following off-site links load properly.</p><p class="m-3"><em>Tip: hold down the “control” key (Windows) or “command” key (Mac) while clicking each link to quickly open them in a series of tabs behind this one.</em></p>
        <ul id="external-links"></ul><p class="m-3">Out-of-context, do these links still make sense? Do they use human-friendly text to accurately describe what they're linking to?</p></div>`;
        document.querySelector('main').appendChild(div);
        externalLinksUl = document.getElementById('external-links');
      }
      // Add each external link to the list for review.
      let li = document.createElement('li');
      // Add index number (w/ leading zeros) as an 'order' attribute so we can sort them.
      const zeroPad = (num, places) => String(num).padStart(places, '0');
      li.setAttribute('order', zeroPad(i, 3));
      li.innerHTML = `<a href="${thisHref}" class="processed">${thisText}</a>`
      externalLinksUl.appendChild(li);
    }
    // After 2, 15, and 30 seconds, sort external links to match their order on the page.
    setTimeout(sortExternalLinks, 2000);
    setTimeout(sortExternalLinks, 15000);
    setTimeout(sortExternalLinks, 30000);
    function sortExternalLinks() {
      var list, i, switching, b, shouldSwitch;
      list = document.getElementById('external-links');
      if (list) {
        switching = true;
        // Make a loop that will continue until no switching has been done:
        while (switching) {
          // Start by saying: no switching is done:
          switching = false;
          b = list.getElementsByTagName('li');
          // Loop through all list items:
          for (i = 0; i < (b.length - 1); i++) {
            // Start by saying there should be no switching:
            shouldSwitch = false;
            // Check if the next item should switch place with the current item:
            if (b[i].getAttribute('order') > b[i + 1].getAttribute('order')) {
              // If next item is alphabetically lower than current item, mark as a switch and break the loop:
              shouldSwitch = true;
              break;
            }
          }
          if (shouldSwitch) {
            // If a switch has been marked, make the switch and mark the switch as done:
            b[i].parentNode.insertBefore(b[i + 1], b[i]);
            switching = true;
          }
        }
      }
    } // sortExternalLinks
  } // buildExternalLinksList

  // A function to build a list of links with document formats.
  function buildDocumentLinksList(thisLink) {
    // Get the link href and text.
    const thisHref = thisLink.getAttribute('href');
    const thisText = thisLink.innerText.trim();
    // Build the containing div and list if it doesn't exist.
    var documentLinksUl = document.getElementById('document-links');
    if (documentLinksUl === null) {
      let div = document.createElement('div'); div.setAttribute('class', 'debug-only border border-success mb-3'); div.innerHTML = `<h2 class="bg-success text-black m-0 p-1">Document Links</h2><p class="m-3">Check the following non-standard HTML pages for accessibility.</p><p class="m-3">Make sure <strong>documents</strong> contains structural tags and image alt text, or a copy of its content is provided as a web page. See tips for <a href='https://webaim.org/techniques/acrobat/'>tagging PDFs</a>, <a href='https://webaim.org/techniques/word/'>Word documents</a>, <a href='https://webaim.org/techniques/powerpoint/'>slideshows</a>, <a href="https://support.microsoft.com/en-us/office/make-your-excel-documents-accessible-to-people-with-disabilities-6cc05fc5-1314-48b5-8eb3-683e49b3e593">spreadsheets</a>, and the <a href="https://inclusivepublishing.org/toolbox/accessibility-guidelines/">EPUB publishing format</a>. Untagged documents often cannot be read by screen readers, and even tagged PDFs and slides may be difficult to read on small screens.</p>
      <ul id="document-links"></ul><p class="m-3">Out-of-context, do these links still make sense? Do they use human-friendly text to accurately describe what they're linking to?</p>`;
      document.querySelector('main').appendChild(div);
      documentLinksUl = document.getElementById('document-links');
    }
    // Add each format link to the list for review.
    let li = document.createElement('li');
    li.innerHTML = `<a href="${thisHref}" class="processed">${thisText}</a>`
    documentLinksUl.appendChild(li);
  } // buildDocumentLinksList

  // A function to build a list of links with audio formats.
  function buildAudioLinksList(thisLink) {
    // Get the link href and text.
    const thisHref = thisLink.getAttribute('href');
    const thisText = thisLink.innerText.trim();
    // Build the containing div and list if it doesn't exist.
    var audioLinksUl = document.getElementById('audio-links');
    if (audioLinksUl === null) {
      let div = document.createElement('div'); div.setAttribute('class', 'debug-only border border-success mb-3'); div.innerHTML = `<h2 class="bg-success text-black m-0 p-1">Audio Links or Embeds</h2><p class="m-3">Check the following linked or embedded audio files for accessibility.</p><p class="m-3">Transcripts are required for all audio content and/or podcasts.</p>
      <ul id="audio-links"></ul><p class="m-3">Out-of-context, do these links still make sense? Do they use human-friendly text to accurately describe what they're linking to?</p>`;
      document.querySelector('main').appendChild(div);
      audioLinksUl = document.getElementById('audio-links');
    }
    // Add each format link to the list for review.
    let li = document.createElement('li');
    li.innerHTML = `<a href="${thisHref}" class="processed">${thisText}</a>`
    audioLinksUl.appendChild(li);
  } // buildAudioLinksList

  // A function to build a list of links with video formats.
  function buildVideoLinksList(thisLink) {
    // Get the link href and text.
    const thisHref = thisLink.getAttribute('href');
    const thisText = thisLink.innerText.trim();
    // Build the containing div and list if it doesn't exist.
    var videoLinksUl = document.getElementById('video-links');
    if (videoLinksUl === null) {
      let div = document.createElement('div'); div.setAttribute('class', 'debug-only border border-success mb-3'); div.innerHTML = `<h2 class="bg-success text-black m-0 p-1">Video Links or Embeds</h2><p class="m-3">Check the following linked or embedded videos for accessibility.</p><p class="m-3">Confirm that all videos provide closed captioning and audio or text description of visual elements. Transcripts are also encouraged.</p>
      <ul id="video-links"></ul>`;
      document.querySelector('main').appendChild(div);
      videoLinksUl = document.getElementById('video-links');
    }
    // Add each format link to the list for review.
    let li = document.createElement('li');
    li.innerHTML = `<a href="${thisHref}" class="processed">${thisText}</a>`
    videoLinksUl.appendChild(li);
  } // buildVideoLinksList

  // A function to build a list of links with image formats.
  function buildImageLinksList(thisLink) {
    // Get the link href and text.
    const thisHref = thisLink.getAttribute('href');
    const thisText = thisLink.innerText.trim();
    // Build the containing div and list if it doesn't exist.
    var imageLinksUl = document.getElementById('image-links');
    if (imageLinksUl === null) {
      let div = document.createElement('div'); div.setAttribute('class', 'debug-only border border-success mb-3'); div.innerHTML = `<h2 class="bg-success text-black m-0 p-1">Image Links</h2><p class="m-3">Check the following linked images for accessibility.</p><p class="m-3">Images and infographics require adequate alt text or text descriptions to describe their content.</p>
      <ul id="image-links"></ul><p class="m-3">Out-of-context, do these links still make sense? Do they use human-friendly text to accurately describe what they're linking to?</p>`;
      document.querySelector('main').appendChild(div);
      imageLinksUl = document.getElementById('image-links');
    }
    // Add each format link to the list for review.
    let li = document.createElement('li');
    li.innerHTML = `<a href="${thisHref}" class="processed">${thisText}</a>`
    imageLinksUl.appendChild(li);
  } // buildImageLinksList
