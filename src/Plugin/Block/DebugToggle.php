<?php

namespace Drupal\debug_content\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Render\Markup;

/**
 * Provides a 'Debug content' toggle block.
 *
 * @Block(
 *   id = "debug_toggle",
 *   admin_label = @Translation("Debug content: toggle debugging styles for authors."),
 *   category = @Translation("Debug Content"),
 * )
 */
class DebugToggle extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    
    // The span with "fas fa-bug" classes is a "bug" icon from Font Awesome (https://fontawesome.com/v5.15/icons/bug?style=solid). You can install Font Awesome, delete or ignore the otherwise empty span, or use the "fas fa-bug" classes to create your own icon styles.
    $markup = t('<span class="fas fa-bug" aria-hidden="true"></span> <a href="#" onclick="return false;" class="debug-content-toggle" role="button" title="Toggle debugging styles for content">Debug content</a>');

    return array(
      '#type' => 'inline_template',
      '#template' => $markup,
      '#attached' => array(
        'library' => array('debug_content/debug-content'),
      ),
    );

  }

}
